import React from 'react';
import { Box, AppBar, Toolbar, Button, Typography } from '@mui/material';

function Header({ loggedIn, onLogin, onLogout }) {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar>
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Signatures List
          </Typography>
          {loggedIn ? (
            <Button color="inherit" variant="outlined" onClick={onLogout}>
              Logout
            </Button>
          ) : (
            <Button color="inherit" variant="outlined" onClick={onLogin}>
              Login
            </Button>
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;
