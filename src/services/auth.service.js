import axios from 'axios';

import TokenService from './token.service';

const TOKEN_UPDATE_TIME_MS = 20000;

let updateTokenTimer;

const login = (username, password) => {
  return new Promise((resolve, reject) => {
    console.log(username);
    console.log(password);
    const params = {
      username,
      password,
      grant_type: 'password',
      client_id: 'corposign-api'
    };
    const data = Object.keys(params)
      .map((key) => `${key}=${encodeURIComponent(params[key])}`)
      .join('&');

    console.log(data);
    const options = {
      method: 'POST',
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
      data,
      url: 'https://my-own-cors-anywhere-proxy.herokuapp.com/https://sso.corposign.net/auth/realms/corposign/protocol/openid-connect/token'
    };
    axios(options)
      .then((res) => {
        updateTokenTimer = setTimeout(updateTokenAuto, TOKEN_UPDATE_TIME_MS);
        localStorage.setItem('loggedIn', true);
        TokenService.setTokensInfo(
          res.data.access_token,
          res.data.expires_in,
          res.data.refresh_token,
          res.data.refresh_expires_in
        );
        resolve({ success: true, data: res.data });
      })
      .catch((err) => {
        console.log(err);
        resolve({ success: false, data: err });
      });
  });
};

const checkIfLoggedIn = () => {
  return new Promise((resolve, reject) => {
    console.log('start cheking');
    if (TokenService.getAccessToken()) {
      if (
        (new Date().getTime() - TokenService.getAccessTokenTimestamp()) / 1000 <
        TokenService.getAccessTokenTimeout() - 5
      ) {
        updateTokenAuto();
        resolve(true);
      } else if (
        (new Date().getTime() - TokenService.getRefreshTokenTimestamp()) /
          1000 <
        TokenService.getRefreshTokenTimestamp() - 5
      ) {
        updateTokenAuto()
          .then((res) => {
            resolve(true);
          })
          .catch((err) => {
            logout();
            resolve(false);
          });
      }
    } else {
      resolve(false);
    }
  });
};

const updateToken = (refresh_token) => {
  return new Promise((resolve, reject) => {
    const params = {
      refresh_token,
      grant_type: 'refresh_token',
      client_id: 'corposign-api'
    };
    const data = Object.keys(params)
      .map((key) => `${key}=${encodeURIComponent(params[key])}`)
      .join('&');

    console.log(data);
    const options = {
      method: 'POST',
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
      data,
      url: 'https://my-own-cors-anywhere-proxy.herokuapp.com/https://sso.corposign.net/auth/realms/corposign/protocol/openid-connect/token'
    };
    axios(options)
      .then((res) => {
        TokenService.setTokensInfo(
          res.data.access_token,
          res.data.expires_in,
          res.data.refresh_token,
          res.data.refresh_expires_in
        );
        resolve({ success: true, data: res.data });
      })
      .catch((err) => {
        console.log(err);
        resolve({ success: false, data: err });
      });
  });
};

const logout = () => {
  localStorage.setItem('loggedIn', false);
  TokenService.clearAll();
  clearTimeout(updateTokenTimer);
};

const updateTokenAuto = () => {
  return new Promise((resolve, reject) => {
    console.log('trying to update token');
    const refreshToken = TokenService.getRefreshToken();
    updateToken(refreshToken).then((res) => {
      if (res.success) {
        TokenService.setTokensInfo(
          res.data.access_token,
          res.data.expires_in,
          res.data.refresh_token,
          res.data.refresh_expires_in
        );
        console.log('successfuly updated token');
        updateTokenTimer = setTimeout(updateTokenAuto, TOKEN_UPDATE_TIME_MS);
        resolve();
      } else {
        console.log('Smth went wrong');
        reject();
      }
    });
  });
};

const AuthService = { login, updateToken, logout, checkIfLoggedIn };

export default AuthService;
