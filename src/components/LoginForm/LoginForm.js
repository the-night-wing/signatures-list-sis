import React, { useState } from 'react';
import { TextField, Stack, Button, Box, LinearProgress } from '@mui/material';
import styles from '../../styles/LoginForm.module.css';

function LoginForm({ onLogin, isLoading, loginResultMessage }) {
  const [email, setEmail] = useState('denysnewemail@gmail.com');
  const [password, setPassword] = useState('strongpass123');

  const onSubmit = (e) => {
    e.preventDefault();
    onLogin(email, password);
  };

  return (
    <Box
      component="form"
      onSubmit={onSubmit}
      noValidate
      className={`${styles.loginForm}`}
    >
      <TextField
        margin="normal"
        required
        fullWidth
        label="Email"
        type="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <TextField
        margin="normal"
        required
        fullWidth
        label="Password"
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      {isLoading ? (
        <LinearProgress sx={{ mt: 3, mb: 2 }} />
      ) : (
        <Button
          type="submit"
          fullWidth
          variant="outlined"
          sx={{ mt: 3, mb: 2 }}
        >
          Log in
        </Button>
      )}
      <Box
        component="span"
        sx={{ display: 'inline-block', width: '100%', textAlign: 'center' }}
      >
        {loginResultMessage}
      </Box>
    </Box>
  );
}

export default LoginForm;
