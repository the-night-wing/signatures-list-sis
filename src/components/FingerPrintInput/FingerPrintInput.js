import React, { useState } from 'react';
import { TextField, Stack, Button, Box } from '@mui/material';

function FingerPrintInput({ onSubmit }) {
  const [fingerPrint, setFingerPrint] = useState(
    '1d3621a51cc7f3c8a802855870bef13f4e26986dc6b57ed89c6663620bae7075'
  );
  const [submitResult, setSubmitResult] = useState(
    'For example 1d3621a51cc7f3c8a802855870bef13f4e26986dc6b57ed89c6663620bae7075'
  );

  const handleChange = (e) => {
    setFingerPrint(e.target.value);
  };

  const submitFingerprint = async () => {
    setSubmitResult('Loading your request');
    const submitStatus = await onSubmit(fingerPrint);
    if (submitStatus.success) {
      setSubmitResult(`Showing ${submitStatus.data} signatures`);
    } else if (submitStatus.data.response.status == 404) {
      setSubmitResult(`Fingerprint is not found`);
    } else if (submitStatus.data.response.status == 401) {
      setSubmitResult(`Login to search for fingerprints`);
    } else {
      setSubmitResult(`Some error occured, try again`);
    }
    console.log(submitStatus);
  };

  return (
    <Stack direction="column" spacing={1} alignItems="center">
      <Stack direction="row" spacing={2}>
        <TextField
          label="fingerPrint"
          variant="outlined"
          value={fingerPrint}
          onChange={handleChange}
          sx={{ width: 400 }}
        ></TextField>
        <Button variant="contained" onClick={submitFingerprint}>
          Check
        </Button>
      </Stack>
      <Box component="span">{submitResult}</Box>
    </Stack>
  );
}

export default FingerPrintInput;
