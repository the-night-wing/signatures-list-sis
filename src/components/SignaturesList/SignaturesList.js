import React, { useState } from 'react';
import List from '@mui/material/List';

import SignaturesListItem from '../SignaturesListItem';

function SignaturesList({ data }) {
  const [openedItems, setOpenedIems] = useState([]);

  const handleClick = (index) => {
    console.log(openedItems);
    if (openedItems.includes(index)) {
      setOpenedIems(openedItems.filter((el) => el !== index));
    } else {
      setOpenedIems([...openedItems, index]);
    }
  };

  return (
    <List>
      {data.map((el, index) => (
        <SignaturesListItem
          key={index}
          handleClick={() => handleClick(index)}
          open={openedItems.includes(index)}
          signatureData={el}
        />
      ))}
    </List>
  );
}

export default SignaturesList;
