const getRefreshToken = () => {
  const refreshToken = localStorage.getItem('refreshToken');
  return refreshToken;
};

const getAccessToken = () => {
  const accessToken = localStorage.getItem('accessToken');
  return accessToken;
};

const setAccessToken = (accessToken) => {
  localStorage.setItem('accessToken', accessToken);
};

const setAccessTokenTimeout = (accessTokenTimeout) => {
  localStorage.setItem('accessTokenTimeout', accessTokenTimeout);
};

const setAccessTokenTimestamp = (accessTokenTimestamp) => {
  localStorage.setItem('accessTokenTimestamp', accessTokenTimestamp);
};

const setRefreshToken = (refreshToken) => {
  localStorage.setItem('refreshToken', refreshToken);
};

const setRefreshTokenTimeout = (refreshTokenTimeout) => {
  localStorage.setItem('refreshTokenTimeout', refreshTokenTimeout);
};

const setRefreshTokenTimestamp = (refreshTokenTimestamp) => {
  localStorage.setItem('refreshTokenTimestamp', refreshTokenTimestamp);
};
const getAccessTokenTimeout = () => {
  return localStorage.getItem('accessTokenTimeout');
};

const getAccessTokenTimestamp = () => {
  return localStorage.getItem('accessTokenTimestamp');
};

const getRefreshTokenTimeout = () => {
  return localStorage.getItem('refreshTokenTimeout');
};

const getRefreshTokenTimestamp = () => {
  return localStorage.getItem('refreshTokenTimestamp');
};

const removeRefreshToken = () => {
  localStorage.removeItem('refreshToken');
};

const removeAccessToken = () => {
  localStorage.removeItem('accessToken');
};
const removeAccessTokenTimeout = () => {
  localStorage.removeItem('accessTokenTimeout');
};

const removeAccessTokenTimestamp = () => {
  localStorage.removeItem('accessTokenTimestamp');
};

const removeRefreshTokenTimeout = () => {
  localStorage.removeItem('refreshTokenTimeout');
};

const removeRefreshTokenTimestamp = () => {
  localStorage.removeItem('refreshTokenTimestamp');
};

const setTokensInfo = (
  accessToken,
  accessTokenTimeout,
  refreshToken,
  refreshTokenTimeout
) => {
  setAccessToken(accessToken);
  setAccessTokenTimeout(accessTokenTimeout);
  setAccessTokenTimestamp(new Date().getTime());
  setRefreshToken(refreshToken);
  setRefreshTokenTimeout(refreshTokenTimeout);
  setRefreshTokenTimestamp(new Date().getTime());
};

const clearAll = () => {
  removeAccessToken();
  removeRefreshToken();
  removeAccessTokenTimeout();
  removeAccessTokenTimestamp();
  removeRefreshTokenTimeout();
  removeRefreshTokenTimestamp();
};

const TokenService = {
  getRefreshToken,
  getRefreshTokenTimeout,
  getRefreshTokenTimestamp,
  getAccessToken,
  getAccessTokenTimeout,
  getAccessTokenTimestamp,
  setAccessToken,
  setAccessTokenTimeout,
  setAccessTokenTimestamp,
  setRefreshToken,
  setRefreshTokenTimeout,
  setRefreshTokenTimestamp,
  removeAccessToken,
  removeRefreshToken,
  setTokensInfo,
  clearAll
};

export default TokenService;
