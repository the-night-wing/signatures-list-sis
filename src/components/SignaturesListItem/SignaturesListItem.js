import React from 'react';
import {
  Collapse,
  List,
  ListItemText,
  ListItemButton,
  ListItemIcon,
  Divider,
  Box
} from '@mui/material';

import { green, red } from '@mui/material/colors';

import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';

import styles from '../../styles/SignatureListItem.module.css';

function SignaturesListItem({
  handleClick,
  open,
  signatureData: {
    signingDate,
    signatureID,
    status,
    notes,
    creatorID,
    creatorMSP
  }
}) {
  return (
    //Put here ListItem for horisontal expanding
    <>
      <Divider />
      <ListItemButton onClick={handleClick}>
        {status === 'accept' ? (
          <ListItemIcon>
            <CheckIcon sx={{ color: green[500] }} />
          </ListItemIcon>
        ) : (
          <ListItemIcon>
            <CloseIcon sx={{ color: red[500] }} />
          </ListItemIcon>
        )}
        <ListItemText primary={signatureID} secondary={`Note: ${notes}`} />
        <ListItemIcon sx={{ justifyContent: 'flex-end' }}>
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItemIcon>
      </ListItemButton>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <Box className={styles['collapse-container']}>
          <List sx={{ width: '50%' }}>
            <Box component="span" className={styles['info-header']}>
              Signature Information
            </Box>
            <Divider sx={{ borderBottomWidth: 3 }} />
            <ListItemText
              primary={new Date(signingDate).toLocaleString()}
              secondary={`Signing Date`}
            />
            <ListItemText primary={status} secondary={`Signature status`} />
          </List>
          <List sx={{ width: '50%' }}>
            <Box component="span" className={styles['info-header']}>
              Signee Information
            </Box>
            <Divider sx={{ borderBottomWidth: 3 }} />
            <ListItemText primary={creatorID} secondary={`Creator ID`} />
            <ListItemText primary={creatorMSP} secondary={`Creator MSP`} />
          </List>
        </Box>
      </Collapse>
    </>
  );
}

export default SignaturesListItem;
